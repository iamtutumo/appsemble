import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  modalTitle: {
    id: 'studio.djM9ag',
    defaultMessage: 'Service Secret',
  },
  docs: {
    id: 'studio.9PjpLU',
    defaultMessage: 'Learn more in the <link>documentation</link>',
  },
  serviceNameLabel: {
    id: 'studio.2bKI2L',
    defaultMessage: 'Service name',
  },
  serviceNameHelp: {
    id: 'studio.uwcEjh',
    defaultMessage: 'An optional name to give extra clarity what the secret is used for.',
  },
  serviceSecretLabel: {
    id: 'studio.v5qzmw',
    defaultMessage: 'URL pattern(s)',
  },
  serviceSecretHelp: {
    id: 'studio.CthkzB',
    defaultMessage: 'The URL pattern(s) used to match against the request action URL.',
  },
  nameLabel: {
    id: 'studio.R9kEeq',
    defaultMessage: 'Secret Name',
  },
  nameHelp: {
    id: 'studio.Y+lmh3',
    defaultMessage: 'The name of the secret to use for the authentication',
  },
  methodLabel: {
    id: 'studio.Vs3jMi',
    defaultMessage: 'Authentication method',
  },
  methodHelp: {
    id: 'studio.1vGmTm',
    defaultMessage: 'The method of authenticating request actions',
  },
  badServiceSecret: {
    id: 'studio.IRIF+a',
    defaultMessage: 'This must be a valid service secret',
  },
  close: {
    id: 'studio.rbrahO',
    defaultMessage: 'Close',
  },
  save: {
    id: 'studio.cPi5ZN',
    defaultMessage: 'Save service secret',
  },
  deleteWarningTitle: {
    id: 'studio.77W3Mq',
    defaultMessage: 'Deleting service secret',
  },
  deleteWarning: {
    id: 'studio.CiHeL3',
    defaultMessage:
      'Are you sure you want to delete this service secret? This action cannot be reverted.',
  },
  cancel: {
    id: 'studio.47FYwb',
    defaultMessage: 'Cancel',
  },
  delete: {
    id: 'studio.UriWmZ',
    defaultMessage: 'Delete service secret',
  },
  deleteSuccess: {
    id: 'studio.cOA7+s',
    defaultMessage: 'Successfully deleted service secret {name}',
  },
  deleteButton: {
    id: 'studio.UriWmZ',
    defaultMessage: 'Delete service secret',
  },
});
